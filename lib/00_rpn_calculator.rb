class RPNCalculator
  @num_stack

  def initialize
    @num_stack = []
  end

  attr_accessor :num_stack, :intermediate_result

  def reset
    @num_stack = []
  end

  def push(num)
    @num_stack.push(num)
  end

  def value
    @num_stack[-1]
  end

  def plus
    if @num_stack.length < 2
      raise "calculator is empty"
    else
      @num_stack.push(@num_stack.pop + @num_stack.pop)
    end
  end

  def minus
    if @num_stack.length < 2
      raise "calculator is empty"
    # if new expression
    else
      subtrahend = @num_stack.pop
      minuend = @num_stack.pop
      @num_stack.push(minuend - subtrahend)
    end
  end

  def divide
    if @num_stack.length < 2
      raise "calculator is empty"
    # if new expression
    else
      divisor = @num_stack.pop * 1.0
      dividend = @num_stack.pop * 1.0
      @num_stack.push(dividend / divisor)
    end
  end

  def times
    if @num_stack.length < 2
      raise "calculator is empty"
    else
      @num_stack.push(@num_stack.pop * @num_stack.pop)
    end
  end

  def tokens(string)
    digits = "1234567890"
    operators = "+-*/"
    result = []

    # check string
    string.split.each do |ch|
      # skip if multiple characters
      if ch.length > 1
        next
      # add digits and operators
      elsif digits.include?(ch)
        result.push(ch.to_i)
      elsif operators.include?(ch)
        result.push(ch.to_sym)
      # reject if none of the above
      else
        next
      end
    end
    result
  end

  def evaluate(string)
    tkns = tokens(string)
    
    while !tkns.empty?
      token = tkns.shift
      case token
      when Integer then push(token)
      when :+ then plus
      when :* then times
      when :- then minus
      when :/ then divide
      end
    end

    value
  end
end
